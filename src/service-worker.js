/* eslint-disable no-undef */
self.__precacheManifest = [].concat(self.__precacheManifest || []);
//workbox.precaching.suppressWarnings();
workbox.precaching.precacheAndRoute(self.__precacheManifest, {});

async function refreshIfWaiting(e){
    if(e.request.mode === 'navigate' && e.request.method === 'GET' && registration.waiting && (await ClientRectList.matchAll()).length < 2) {
        e.respondWith(() => {       
            registration.waiting.postMessage('skipWaiting');
            return new Response("", {headers: {"Refresh": "0"}});
        });
    }
}

// Check all requests
self.addEventListener('fetch', e => {
    console.log('SW request: ', e.request);
    // TODO: Internal sites start with /show/#. Need to add auth code/token
    // Auto-refresh SW
    refreshIfWaiting(e);
});

// Check Messages
self.addEventListener('message', (event) => {
    if(event.data) {
        if (event.data.type === 'SKIP_WAITING') {
            self.skipWaiting();
        } else if (event.data.type === 'UPDATE_LIST') {
            event.data.list.forEach(url => {
                workbox.routing.registerRoute(
                    url,
                    new workbox.strategies.CacheFirst({
                        cacheName: 'website-cache',
                        matchOptions: {
                            ignoreVary: true,
                        },
                        plugins: [
                            new workbox.expiration.ExpirationPlugin({
                                maxEntries: 500,
                                maxAgeSeconds: 300,
                                purgeOnQuotaError: true,
                            }),
                            new workbox.cacheableResponse.CacheableResponsePlugin({
                                statuses: [0, 200],
                            }),
                        ],
                    })
                );
            });
        }
    }
});
