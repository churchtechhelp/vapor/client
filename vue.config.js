const CompressionPlugin = require('compression-webpack-plugin');
const zlib = require('zlib');

module.exports = {
    pwa: {
        workboxPluginMode: 'InjectManifest',
        workboxOptions: {
            swSrc: 'src/service-worker.js',
        },
        msTileColor: '#ffc40d',
        themeColor: '#333333',
        appleMobileWebCapable: 'yes',
        appleMobileWebAppStatusBarStyle: 'black',
        iconPaths: {
          favicon32: 'favicon-32x32.png',
          favicon16: 'favicon-16x16.png',
          appleTouchIcon: 'apple-touch-icon.png',
          maskIcon: 'safari-pinned-tab.svg',
        },
        manifestOptions: {
          "name": "Vapor Client",
          "short_name": "Vapor-Client",
          "icons": [
            {
              "src": "/android-chrome-192x192.png",
              "sizes": "192x192",
              "type": "image/png"
            },
            {
              "src": "/android-chrome-512x512.png",
              "sizes": "512x512",
              "type": "image/png"
            }
          ],
          "theme_color": "#333333",
          "background_color": "#000000",
          "display": "standalone"
      }
    },
    configureWebpack: {
        plugins: (process.env.NODE_ENV === 'production')? [
            new CompressionPlugin({
              filename: '[path][base].gz',
              algorithm: 'gzip',
              test: /\.js$|\.css$|\.html$/,
              threshold: 10240,
              minRatio: 0.8,
            }),
            new CompressionPlugin({
              filename: '[path][base].br',
              algorithm: 'brotliCompress',
              test: /\.(js|css|html|svg)$/,
              compressionOptions: {
                params: {
                  [zlib.constants.BROTLI_PARAM_QUALITY]: 11,
                },
              },
              threshold: 10240,
              minRatio: 0.8,
            }),
          ]:[],
    },
};
