module.exports = {
  future: {
    removeDeprecatedGapUtilities: true,
    purgeLayersByDefault: true,
  },
  purge: [
    "./src/**/*.html",
    "./src/**/*.vue",
  ],
  theme: {
    extend: {
      fontSize: {
        '2xs': '0.65rem',
        '7xl': '6rem',
        '8xl': '7rem',
        '9xl': '10rem',
        '10xl': '12rem',
      }
    },
  },
  variants: {},
  plugins: [],
}
